# TODO

* Rails
  * ~~Video model~~
    * ~~on destroy callbacks~~
  * Video controller w/ all actions
  * ~~listener for converter messages~~
  * ~~move converted file w/ proper tree dir structure~~
  * ~~delete source~~

* frontend
  * uploading status
  * converting status

* uploading files
  * ~~basic upload~~
  * pre filter by mime type/extension
  * upload progressbar

* converting
  * ~~converting daemon~~
  * ~~rabbitmq messaging~~
  * ~~ffmpeg wrapper~~
  * ~~smart scheduling~~
  * ~~progress detection~~
  * ~~notifications on convertion~~
  * limit max width (w/ proper aspect)
  * ~~two-pass encoding~~
  * ~~robust error handling~~

* nginx
  * ~~kaltura~~
  * kaltura with playlists
  * security tokens

* playback
  * videojs
  * ~~adaptive bitrate~~

* beautify
  * styles
