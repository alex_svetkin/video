BACKEND_MYSQL_USERNAME = 'backend'
BACKEND_MYSQL_PASSWORD = 'pass'
BACKEND_RABBITMQ_USERNAME = 'backend'
BACKEND_RABBITMQ_PASSWORD = 'pass'

Vagrant.configure('2') do |config|
  config.vm.define 'backend' do |config|
    config.vm.box = 'bento/ubuntu-16.04'
    config.vm.hostname = 'vkv-backend'
    config.vm.network 'forwarded_port', guest: 3000, host: 3000
    config.vm.network 'forwarded_port', guest: 4000, host: 4000
    config.vm.network 'forwarded_port', guest: 15672, host: 15672
    config.ssh.forward_agent = true

    config.vm.provider 'virtualbox' do |v|
      v.memory = 4096
      v.cpus = 4
    end
  end

  config.vm.provision 'ruby', type: 'shell', inline: <<-SHELL
    echo 'export VAGRANT=1' > /etc/profile.d/vagrant.sh
    export DEBIAN_FRONTEND=noninteractive
    apt-get update && apt-get -y upgrade && apt-get -y install ntp build-essential zlib1g-dev \
      libssl-dev libreadline6-dev libyaml-dev \
      nodejs-legacy \
      git
    useradd -U rbenv || true
    usermod -G rbenv -a vagrant
    git clone https://github.com/rbenv/rbenv.git /usr/local/rbenv
    cd /usr/local/rbenv && ./src/configure && make -C src || true
    echo 'export PATH="/usr/local/rbenv/bin:$PATH"' > /etc/profile.d/rbenv.sh
    echo 'export RBENV_ROOT="/usr/local/rbenv"' >> /etc/profile.d/rbenv.sh
    echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh
    git clone https://github.com/rbenv/ruby-build.git /usr/local/rbenv/plugins/ruby-build
    source /etc/profile.d/rbenv.sh
    rbenv install 2.3.1
    rbenv global 2.3.1
    chgrp -R rbenv /usr/local/rbenv
    chmod -R g+rwxXs /usr/local/rbenv
    gem install bundler
  SHELL

  config.vm.provision 'converter', type: 'shell', inline: <<-SHELL
    export DEBIAN_FRONTEND=noninteractive
    apt-get install software-properties-common
    add-apt-repository ppa:jonathonf/ffmpeg-3
    apt-get update && apt-get install ffmpeg
  SHELL

  config.vm.provision 'mysql', type: 'shell', inline: <<-SHELL
    export DEBIAN_FRONTEND=noninteractive
    apt-get update && apt-get install mysql-server mysql-client libmysqlclient-dev
    #echo 'character-set-server = utf8' >> /etc/mysql/mysql.conf.d/mysqld.cnf
    #echo 'collation-server = utf8_general_ci' >> /etc/mysql/mysql.conf.d/mysqld.cnf
    # TODO: create user and databases (development, test or production)
  SHELL

  config.vm.provision 'rabbitmq', type: 'shell', inline: <<-SHELL
    export DEBIAN_FRONTEND=noninteractive
    echo 'deb http://www.rabbitmq.com/debian/ testing main' | \
      tee /etc/apt/sources.list.d/rabbitmq.list
    wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc |
      apt-key add -
    apt-get update && apt-get install -y rabbitmq-server
    # Rabbit
    rabbitmq-plugins enable rabbitmq_management
    wget -O /usr/local/bin/rabbitmqadmin "http://localhost:15672/cli/rabbitmqadmin" && \
      chmod a+x /usr/local/bin/rabbitmqadmin
    rabbitmqctl add_user root NTQ4YzZiZjJjMmVh && \
      rabbitmqctl set_user_tags root administrator && \
      rabbitmqctl set_permissions -p / root ".*" ".*" ".*"
  SHELL

  config.vm.provision 'redis', type: 'shell', inline: <<-SHELL
    export DEBIAN_FRONTEND=noninteractive
    apt-get update && apt-get install redis-server
  SHELL

  # nginx
  # ./configure --add-module=/usr/local/src/nginx-vod-module/ --with-threads --with-cc-opt="-O3"
end
