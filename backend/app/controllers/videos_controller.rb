class VideosController < ApplicationController
  def index
    @videos = Video.all
  end

  def progress
    @video = Video.find_by_id(params[:id])
    respond_to do |format|
      format.html
      format.json do
        render json: {
          status_humanized: @video.status_humanized,
          renditions: @video.renditions
        }
      end
    end
  end

  def streaming_progress
  end

  def show
    @video = Video.find(params[:id])
  end

  def new
    @video = Video.new
  end

  def create
    @video = Video.new
    @video._source = params.fetch(:video, {})[:source]

    if @video.save
      flash.now[:notice] = 'Video uploaded'
      redirect_to progress_video_path(@video)
    else
      flash.now[:error] = @video.errors.values.join(', ')
      render :new
    end
  end

  def destroy
    video = Video.find(params[:id])
    if video.destroy
      flash[:notice] = 'Video deleted'
    else
      flash[:error] = "Failed to delete Video##{video.id}"
    end

    redirect_to videos_path
  end
end
