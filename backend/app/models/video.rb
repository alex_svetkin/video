# == Schema Information
#
# Table name: videos
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  source     :string(255)
#  duration   :integer
#  status     :integer
#  a128k      :integer
#  v360p      :integer
#  v480p      :integer
#  v720p      :integer
#  v1080p     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Video < ApplicationRecord
  include RabbitMQ

  # just a list of all rendition names
  RENDITIONS = %i[a128k v360p v480p v720p v1080p].freeze

  # video renditions options
  VIDEO_PARAMS = {
    v360p:  { height: 360,  bitrate: '500k', required: true },
    v480p:  { height: 480,  bitrate: '1000k' },
    v720p:  { height: 720,  bitrate: '2000k', two_pass: true, threads: 2 },
    v1080p: { height: 1080, bitrate: '3000k', two_pass: true, threads: 2 }
  }.freeze
  # group of pictures size (frames), must be the same for all renditions
  GOP_SIZE = 100

  STATUS_NONE    = 0
  STATUS_OK      = 1
  STATUS_PENDING = 2
  STATUS_ERROR   = 3
  STATUS_MISSING = 4
  STATUS_UNKNOWN = 5

  INV_STATUS_MAP = { STATUS_NONE => :none, STATUS_OK => :ok, STATUS_PENDING => :pending, STATUS_ERROR => :error, STATUS_MISSING => :missing, STATUS_UNKNOWN => :unknown }.freeze

  validate :uploaded_file_must_be_valid, on: :create
  validate :video_file_must_be_valid, on: :create
  validate :video_renditions_must_be_valid, on: :create

  after_initialize :defaults
  before_create :handle_source
  after_create :notify_converter

  after_update :publish_converted_video, if: -> { status_changed? && status == STATUS_OK }
  after_destroy :remove_files

  # mounter for source attribute
  # TODO: see carrierwaveuploader (https://github.com/carrierwaveuploader/carrierwave/blob/master/lib/carrierwave/mount.rb)
  # for a nicer implementation of a 'mounter'
  attr_accessor :_source

  # @param [Symbol] name
  def update_rendition_status!(name, status)
    self[name] = status
    self.status = infere_main_status
    save!
  end

  # @return [Boolean]
  def ok?
    status == STATUS_OK
  end

  # @return [Boolean]
  def pending?
    status == STATUS_PENDING
  end

  # @return [Boolean]
  def error?
    status == STATUS_ERROR
  end

  # @return [Boolean]
  def hd?
    v720p > STATUS_NONE || v1080p > STATUS_NONE
  end

  # @return [Boolean]
  def sd?
    !hd?
  end

  # @return [Integer]
  def duration_in_seconds
    (duration / 1000).to_i
  end

  def status_humanized
    INV_STATUS_MAP.fetch(status)
  end

  # renditions statuses and progress, if available
  # @return [Array<Hash>]
  def renditions
    result = []
    RENDITIONS.each do |name|
      next if self[name] == STATUS_NONE
      progress, time_left = rendition_progress(name)
      result << {
        name: name,
        status: self[name],
        status_humanized: INV_STATUS_MAP.fetch(self[name]),
        progress_in_percents: progress ? (progress * 100).to_i : nil,
        time_left: time_left ? time_left.to_i : nil
      }
    end
    result
  end

  private

  attr_accessor :_movie, :_renditions

  def defaults
    self.source ||= SecureRandom.uuid
  end

  def uploaded_file_must_be_valid
    if _source.blank?
      errors.add(:source, 'No file uploaded')
    elsif !_source.is_a?(ActionDispatch::Http::UploadedFile)
      errors.add(:source, "Expected ActionDispatch::Http::UploadedFile object, got #{_source.class}")
    elsif _source.size == 0
      errors.add(:source, 'Empty file')
    end
  end

  def video_file_must_be_valid
    return unless errors[:source].blank?

    self._movie = FFMPEG::Movie.new(_source.path)
    logger.info "Got movie: #{_movie.inspect}"
    errors.add(:source, 'Invalid video file') unless _movie.valid?
    errors.add(:source, 'No video stream found') unless _movie.video_stream
    errors.add(:source, 'No video height') unless _movie.height
  rescue ActiveRecord::ActiveRecordError
    errors.add(:source, "Error: #{e}")
  rescue StandardError
    raise
  end

  def video_renditions_must_be_valid
    return unless errors[:source].blank?

    self._renditions = detect_all_renditions(_movie)
    logger.info "Renditions: #{_renditions.inspect}"
    errors.add(:source, 'No suitable renditions for this video file') if _renditions.empty?
  rescue ActiveRecord::ActiveRecordError
    errors.add(:source, "Error: #{e}")
  rescue StandardError
    raise
  end

  def handle_source
    raise 'No _source found' if _source.blank?

    handle_uploaded_file
    init_movie_info
    init_statuses
  end

  def handle_uploaded_file
    dst = Pathname.new(SETTINGS[:converter][:input_path]) + source
    move_uploaded_file(_source.path, dst)
    self.name = _source.original_filename
  end

  def init_movie_info
    self.duration = (_movie.duration * 1000).to_i
  end

  def init_statuses
    avail_renditions = Set.new(_renditions.map { |f| f[:name] })
    puts avail_renditions.inspect
    RENDITIONS.each do |name|
      puts name.inspect
      self[name] = avail_renditions.include?(name) ? STATUS_PENDING : STATUS_NONE
    end
    self.status = STATUS_PENDING
  end

  def move_uploaded_file(src, dst)
    FileUtils.mv(src, dst)
  end

  # TODO: split method
  def publish_converted_video
    # ensure target path is created
    FileUtils.mkdir_p(renditions_dir)

    RENDITIONS.each do |name|
      next if self[name] == STATUS_NONE # skip if we don't have that rendition
      raise 'Unexpected rendition status' if self[name] != STATUS_OK
      src = Pathname.new(SETTINGS[:converter][:output_path]).join(rendition_file_name(name))
      unless src.readable?
        # instant update, skip validations
        update_column(name, STATUS_MISSING)
        update_column(:status, STATUS_MISSING)
        raise "Rendition #{src} not found for Video##{id}"
      end
      dst = rendition_full_path(name)
      logger.debug "Moving rendition #{name}: #{src} -> #{dst}..."
      begin
        FileUtils.mv(src, dst)
      rescue StandardError => e
        logger.error "Failed to move rendition #{name}: #{e}"
      end
    end

    remove_source
  end

  def remove_source
    logger.debug "Removing source for Video##{id}..."
    FileUtils.rm(Pathname.new(SETTINGS[:converter][:input_path]) + source)
  end

  def infere_main_status
    # get all renditions statuses, drop NONEs
    rendition_statuses = RENDITIONS.map { |name| self[name] }.reject { |s| s == STATUS_NONE }
    # none          -> NONE
    return STATUS_NONE if rendition_statuses.size == 0
    # all OK        -> OK
    return STATUS_OK if rendition_statuses.all? { |s| s == STATUS_OK }
    # any ERROR     -> ERROR
    return STATUS_ERROR if rendition_statuses.any? { |s| s == STATUS_ERROR }
    # any PENDING   -> PENDING
    return STATUS_PENDING if rendition_statuses.any? { |s| s == STATUS_PENDING }
    # anything else -> UNKNOWN
    STATUS_UNKNOWN
  end

  def notify_converter
    raise 'No target renditions' unless _renditions

    with_rabbitmq do |channel|
      _renditions.each do |rendition|
        msg = notification_body(rendition)
        channel.default_exchange.publish(msg, routing_key: 'converter_in')
      end
    end
  end

  def detect_all_renditions(movie)
    raise 'Not an FFMPEG::Movie' unless movie.is_a? FFMPEG::Movie

    detect_video_renditions(movie) + detect_audio_renditions(movie)
  end

  def detect_video_renditions(movie)
    return [] unless movie.height

    renditions = []
    VIDEO_PARAMS.each do |name, params|
      break if movie.height < params.fetch(:height) && !params.fetch(:required, false)

      renditions << {
        type: :video,
        name: name,
        height: params.fetch(:height),
        vb: params.fetch(:bitrate),
        two_pass: params.fetch(:two_pass, false),
        gop_size: GOP_SIZE,
        preset: params.fetch(:preset, 'fast'),
        threads: params.fetch(:threads, 1),
        filename: rendition_file_name(name)
      }
    end

    renditions
  end

  def detect_audio_renditions(movie)
    return [] unless movie.audio_stream
    [{
      type: :audio,
      name: :a128k,
      ab: '128k',
      filename: rendition_file_name(:a128k)
    }]
  end

  def notification_body(rendition)
    raise 'Unknown rendition type' unless [:video, :audio].include?(rendition[:type])

    JSON.dump(
      id: id,
      source: source,
      rendition: rendition
    )
  end

  def rendition_full_path(name)
    Pathname.new(renditions_dir).join(rendition_file_name(name)).to_s
  end

  def renditions_dir
    raise 'No directories without source name' if source.blank?
    dir1 = source.slice(0, 2)
    dir2 = source.slice(2, 2)
    Pathname.new(SETTINGS[:video][:path]).join(dir1).join(dir2).to_s
  end

  def rendition_file_name(name)
    raise 'No rendition name without source name' if source.blank?
    source + "-#{name}.mp4"
  end

  # @return [Tuple<Float, Integer?>]
  def rendition_progress(name)
    progress, time_left = REDIS.get(rendition_file_name(name)).split('|')
    [progress.to_f, time_left]
  rescue StandardError => e
    logger.warn "Failed to get rendition '#{name}' progress: #{e}"
    [nil, nil]
  end

  def remove_files
    RENDITIONS.each do |name|
      next if self[name] == STATUS_NONE

      path = rendition_full_path(name)
      begin
        FileUtils.rm(path)
      rescue StandardError => e
        logger.warn "Failed to remove rendition #{path}: #{e}"
      end
    end
  end
end
