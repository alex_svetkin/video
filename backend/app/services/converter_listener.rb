class ConverterListener
  include RabbitMQ

  SLEEPING_DELAY = 5

  def initialize
    Signal.trap('INT') { throw :abort, 'Got SIGINT' }
    Signal.trap('TERM') { throw :abort, 'Got SIGTERM' }

    @logger = Logger.new(STDOUT)
    @logger.level = :debug
    @channel = RABBITMQ.channel
    @queue = @channel.queue('converter_out', durable: true, exclusive: false)
  end

  def start!
    @logger.debug 'Starting ConverterListener...'

    create_consumer do |delivery_info, _properties, payload|
      begin
        @logger.info "Got message: #{payload.inspect}"

        if process(payload)
          @logger.info 'Successfully processed.'
          @channel.ack(delivery_info.delivery_tag)
        else
          @logger.info 'Failed to process.'
          @channel.nack(delivery_info.delivery_tag)
        end
      rescue StandardError => e
        @logger.error "Failed to process: #{e} #{e.backtrace.join("\n")}"
        @channel.nack(delivery_info.delivery_tag)
      end
    end

    error_message = catch(:abort) do
      loop do
        @logger.debug 'Nothing to do...'
        sleep SLEEPING_DELAY
      end
    end
    @logger.info "Exited with error message: #{error_message}"
  rescue StandardError => e
    @logger.error e
  ensure
    finalize
  end

  private

  def create_consumer
    @logger.debug 'Creating consumer...'
    @queue.subscribe(manual_ack: true) do |delivery_info, properties, payload|
      yield delivery_info, properties, payload
    end
  end

  def process(payload)
    msg = JSON.parse(payload)
    video = Video.find(msg['id'])
    # bit awkward as statuses constants must match exactly
    video.with_lock do
      video.update_rendition_status!(msg['rendition']['name'].to_sym, msg['rendition']['status'].to_i)
    end
    @logger.debug "Video##{video.id}: #{video.inspect}"
  end

  def finalize
    @logger.debug 'Finalizing...'
    @channel.close
  end
end
