class VideoLink
  def self.insecure_url(video)
    dir1 = video.source.slice(0, 2)
    dir2 = video.source.slice(2, 2)

    prefix = video.source + '-'
    middle = video.renditions.map { |r| r[:name] }.join(',')
    postfix = '.mp4'
    extra = '.urlset/master.m3u8'

    SETTINGS[:video][:base_url] \
      + dir1 + '/' + dir2 + '/' \
      + prefix + ',' + middle + ',' + postfix + extra
  end
end
