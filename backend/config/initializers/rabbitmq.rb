config = YAML.load(ERB.new(File.read(Rails.root.join('config/rabbitmq.yml'))).result)[Rails.env].with_indifferent_access

RABBITMQ = Bunny.new(
  host:  config[:host],
  port:  config[:port],
  vhost: config[:vhost],
  user:  config[:user],
  pass:  config[:pass]
)

RABBITMQ.start

module RabbitMQ
  def with_rabbitmq
    channel = RABBITMQ.create_channel
    yield channel
    channel.close
  end
end