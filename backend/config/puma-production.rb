environment 'production'
workers 2
threads 1, 4
bind 'tcp://127.0.0.1:4000'
daemonize
stdout_redirect './log/puma.stdout.log', './log/puma.stderr.log', true
