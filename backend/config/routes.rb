Rails.application.routes.draw do
  resources :videos, except: [:edit, :update] do
    member do
      get :progress
    end
  end

  root to: 'videos#index'
end
