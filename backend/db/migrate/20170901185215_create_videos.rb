class CreateVideos < ActiveRecord::Migration[5.1]
  def change
    create_table :videos do |t|
      t.string :name
      t.string :source
      t.integer :duration
      t.integer :status, limit: 1
      t.integer :a128k, limit: 1
      t.integer :v360p, limit: 1
      t.integer :v480p, limit: 1
      t.integer :v720p, limit: 1
      t.integer :v1080p, limit: 1

      t.timestamps
    end
  end
end
