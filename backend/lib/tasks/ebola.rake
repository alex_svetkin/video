desc 'drop all video'
task ebola: [:environment] do
  puts 'Are you sure to destroy all videos and files? (y/N)'
  exit(0) unless STDIN.gets.strip.downcase == 'y'

  Video.destroy_all

  `/usr/bin/find '#{SETTINGS[:video][:path]}' -type d -empty -delete`
  `/usr/bin/find '#{SETTINGS[:converter][:input_path]}' -type f -delete`
  `/usr/bin/find '#{SETTINGS[:converter][:output_path]}' -type f -delete`
end
