FactoryGirl.define do
  factory :video do
    name 'test video'
    source '12345678-abcd-0000-0000-1234567890ab'
    status  Video::STATUS_OK
    a128k   Video::STATUS_OK
    v360p   Video::STATUS_OK
    v480p   Video::STATUS_OK
    v720p   Video::STATUS_OK
    v1080p  Video::STATUS_OK

    to_create { |instance| instance.save(validate: false) }

    before(:create) do |video|
      # supress callbacks
      video.define_singleton_method(:handle_source) {}
      video.define_singleton_method(:notify_converter) {}
      video.define_singleton_method(:publish_converted_video) {}
      video.define_singleton_method(:remove_files) {}
    end
  end
end
