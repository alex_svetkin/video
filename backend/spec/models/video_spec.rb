describe Video do
  let(:video) { create :video }
  it 'is valid with valid attributes' do
    expect(video).to be_valid
  end

  describe '#notification_body' do
    it 'returns valid JSON' do
      rendition = { type: :video, other: 'stuff' }
      expect(JSON.parse(video.send(:notification_body, rendition))).to eq(
        'id' => video.id,
        'source' => video.source,
        'rendition' => { 'type' => 'video', 'other' => 'stuff' }
      )
    end
  end

  describe '#renditions' do
    it 'returns renditions statuses and progress' do
      allow_any_instance_of(Video).to receive(:rendition_progress).and_return([0.5, 30])
      expect(video.renditions).to eq(
        [
          { name: :a128k, status: 1, status_humanized: :ok, progress_in_percents: 50, time_left: 30 },
          { name: :v360p, status: 1, status_humanized: :ok, progress_in_percents: 50, time_left: 30 },
          { name: :v480p, status: 1, status_humanized: :ok, progress_in_percents: 50, time_left: 30 },
          { name: :v720p, status: 1, status_humanized: :ok, progress_in_percents: 50, time_left: 30 },
          { name: :v1080p, status: 1, status_humanized: :ok, progress_in_percents: 50, time_left: 30 }
        ]
      )
    end
  end

  describe '#update_rendition_status!' do
    let(:video) do
      video = create :video, status: Video::STATUS_PENDING
      Video::RENDITIONS.each { |name| video[name] = Video::STATUS_OK }
      video
    end

    it 'sets main status to OK if all are OK' do
      video.a128k = Video::STATUS_PENDING
      expect { video.update_rendition_status!(:a128k, Video::STATUS_OK) }
        .to change { video.status }.to(Video::STATUS_OK)
    end

    it 'do not change main status if some are pending' do
      video.a128k = Video::STATUS_PENDING
      video.v360p = Video::STATUS_PENDING
      expect { video.update_rendition_status!(:a128k, Video::STATUS_OK) }
        .to_not change { video.status }
    end

    it 'sets main status to ERROR if one is ERROR' do
      video.a128k = Video::STATUS_PENDING
      expect { video.update_rendition_status!(:a128k, Video::STATUS_ERROR) }
        .to change { video.status }.to(Video::STATUS_ERROR)
    end
  end

  describe '#detect_audio_renditions' do
    it 'returns array of audio renditions' do
      movie = double
      allow(movie).to receive(:audio_stream) { true }

      expect(video.send(:detect_audio_renditions, movie)).to eq [
        {
          name: :a128k,
          type: :audio,
          ab: '128k',
          filename: '12345678-abcd-0000-0000-1234567890ab-a128k.mp4'
        }
      ]
    end
  end

  describe '#detect_video_renditions' do
    it 'returns all video renditions for a high quality movie' do
      movie = double
      allow(movie).to receive(:height) { 1080 }

      expect(video.send(:detect_video_renditions, movie)).to eq [
        {
          type: :video,
          name: :v360p,
          height: 360,
          vb: '500k',
          two_pass: false,
          gop_size: Video::GOP_SIZE,
          preset: 'fast',
          threads: 1,
          filename: '12345678-abcd-0000-0000-1234567890ab-v360p.mp4'
        },
        {
          type: :video,
          name: :v480p,
          height: 480,
          vb: '1000k',
          two_pass: false,
          gop_size: Video::GOP_SIZE,
          preset: 'fast',
          threads: 1,
          filename: '12345678-abcd-0000-0000-1234567890ab-v480p.mp4'
        },
        {
          type: :video,
          name: :v720p,
          height: 720,
          vb: '2000k',
          two_pass: true,
          gop_size: Video::GOP_SIZE,
          preset: 'fast',
          threads: 2,
          filename: '12345678-abcd-0000-0000-1234567890ab-v720p.mp4'
        },
        {
          type: :video,
          name: :v1080p,
          height: 1080,
          vb: '3000k',
          two_pass: true,
          gop_size: Video::GOP_SIZE,
          preset: 'fast',
          threads: 2,
          filename: '12345678-abcd-0000-0000-1234567890ab-v1080p.mp4'
        }
      ]
    end

    it 'returns the lowest rendition for a very low quality movie' do
      movie = double
      allow(movie).to receive(:height) { 240 }

      expect(video.send(:detect_video_renditions, movie)).to eq [
        {
          type: :video,
          name: :v360p,
          height: 360,
          vb: '500k',
          two_pass: false,
          gop_size: Video::GOP_SIZE,
          preset: 'fast',
          threads: 1,
          filename: '12345678-abcd-0000-0000-1234567890ab-v360p.mp4'
        }
      ]
    end
  end

  describe '#rendition_full_path' do
    it 'returns full path to a rendition' do
      expect(video.send(:rendition_full_path, :a128k))
        .to eq '/var/video/12/34/12345678-abcd-0000-0000-1234567890ab-a128k.mp4'
    end
  end
end
