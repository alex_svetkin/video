describe ConverterListener do
  let(:listener) do
    ConverterListener.new
  end

  describe '#process' do
    it 'should update rendition status' do
      video = create :video, a128k: Video::STATUS_PENDING

      message = JSON.dump(id: video.id, rendition: { name: :a128k, status: Video::STATUS_OK })
      expect { listener.send(:process, message) }.to change { video.reload.a128k }.to(Video::STATUS_OK)
    end

    it 'should update main status too' do
      video = create :video, status: Video::STATUS_PENDING, a128k: Video::STATUS_PENDING
      # supress callback
      allow_any_instance_of(Video).to receive(:publish_converted_video)

      message = JSON.dump(id: video.id, rendition: { name: :a128k, status: Video::STATUS_OK })
      expect { listener.send(:process, message) }.to change { video.reload.status }.to(Video::STATUS_OK)
    end
  end
end
