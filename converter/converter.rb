#!/usr/bin/env ruby
require 'optparse'
require 'yaml'
require 'erb'
require 'logger'

require './lib/server'

options = {}
OptionParser.new do |opts|
  opts.banner = 'Usage: converter.rb [options]'

  options[:environment] = 'development'
  opts.on('-e', '--environment [ENV]', 'Environment name') do |v|
    options[:environment] = v
  end

  options[:config] = 'config.yml'
  opts.on('-c', '--config', 'Config file path') do |v|
    options[:config] = v
  end

  options[:loglevel] = 'debug'
end.parse!

SETTINGS = YAML.load(ERB.new(File.read(options[:config])).result)[options[:environment]]
LOGGER = Logger.new(STDOUT)
LOGGER.level = options[:loglevel].to_sym

converter = Server.new
converter.start!
