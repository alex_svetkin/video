require './lib/worker'

class Cluster
  def initialize
    @workers = []
  end

  # @param [Boolean] is overloaded?
  def full?
    workload >= SETTINGS['cpus'].to_i
  end

  # @param [Integer] all workers workload
  def workload
    @workers.inject(0) { |sum, worker| sum + worker.workload }
  end

  # drop dead workers
  def refresh!
    @workers = @workers.select(&:status)
  end

  # create and add worker for processing the given message
  # @param [Message] message
  # @param [Proc] progress_callback
  # @param [Proc] finish_callback
  def add_worker(message, progress_callback, finish_callback)
    _add_worker(Worker.new(message, progress_callback, finish_callback))
  end

  private

  # add worker
  # @param [Worker] worker
  def _add_worker(worker)
    @workers << worker
  end
end
