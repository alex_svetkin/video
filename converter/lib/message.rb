require 'json'

class Message
  attr_reader :delivery_info, :properties, :payload, :body

  # @param [Hash] delivery_info
  # @param [Hash] properties
  # @param [String] payload
  def initialize(delivery_info, properties, payload)
    @delivery_info = delivery_info
    @properties = properties
    @payload = payload
    @body = JSON.parse(payload)
  end

  # @return [Boolean]
  def empty?
    payload.nil? || payload == '' || payload == '{}'
  end

  # @return [Boolean]
  def valid?
    true
  end

  # @return [Integer]
  def delivery_tag
    delivery_info.delivery_tag
  end
end
