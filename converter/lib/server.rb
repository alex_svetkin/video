require 'bunny'
require 'redis'

require './lib/message.rb'
require './lib/cluster.rb'

class Server
  SLEEPING_DELAY = 5
  RENDITION_PROGRESS_TTL = 60

  def initialize
    Signal.trap('INT') { throw :abort, 'Got SIGINT' }
    Signal.trap('TERM') { throw :abort, 'Got SIGTERM' }

    @redis = Redis.new(url: SETTINGS['redis'])
    @rabbitmq = create_rabbitmq_connection
    @channel = @rabbitmq.create_channel
    @queue = @channel.queue('converter_in', durable: true, exclusive: false)
    @cluster = Cluster.new
  end

  def start!
    LOGGER.debug 'Starting converter server...'

    error_message = catch(:abort) do
      loop do
        @cluster.refresh!

        if @cluster.full?
          LOGGER.debug 'At full load, sleeping...'
          sleep SLEEPING_DELAY
        else
          message = fetch_message
          if message.nil?
            LOGGER.debug 'Nothing to do...'
            sleep SLEEPING_DELAY
          elsif message.valid?
            @cluster.add_worker(message, method(:notify_on_progress), method(:notify_on_finish))
          else
            LOGGER.warn 'Got invalid message, rejecting'
            @channel.nack(message[0].delivery_tag)
          end
        end
      end
    end
    LOGGER.info "Exited with error message: #{error_message}"
  rescue StandardError => e
    LOGGER.error e
  ensure
    finalize
  end

  private

  def fetch_message
    LOGGER.debug 'Retrieving message...'
    delivery_info, properties, payload = @queue.pop(manual_ack: true)
    if delivery_info.nil?
      nil
    else
      LOGGER.debug "Got message: #{payload}"
      Message.new(delivery_info, properties, payload)
    end
  end

  def notify_on_finish(message, result)
    if result
      notify_on_success(message)
      @channel.ack(message.delivery_tag)
    else
      notify_on_failure(message)
      @channel.nack(message.delivery_tag)
    end
  end

  # TODO:
  def notify_on_progress(rendition, progress, time_left)
    LOGGER.debug "#{rendition.inspect}: #{progress}, #{time_left}"
    val = "#{progress}|#{time_left}"
    @redis.set(rendition_progress_key(rendition), val, ex: RENDITION_PROGRESS_TTL)
  end

  def rendition_progress_key(rendition)
    rendition['filename']
  end

  def notify_on_success(message)
    LOGGER.info "Converted."
    message.body['rendition']['status'] = 1
    out_msg = JSON.dump(message.body)
    LOGGER.debug "Sending message: #{out_msg}"
    @channel.default_exchange.publish(out_msg, routing_key: 'converter_out')
  end

  def notify_on_failure(message)
    LOGGER.info "Failed to convert."
    message.body['rendition']['status'] = 3
    out_msg = JSON.dump(message.body)
    LOGGER.debug "Sending message: #{out_msg}"
    @channel.default_exchange.publish(out_msg, routing_key: 'converter_out')
  end

  def create_rabbitmq_connection
    conn = Bunny.new(
      host:  SETTINGS['rabbitmq']['host'],
      port:  SETTINGS['rabbitmq']['port'],
      vhost: SETTINGS['rabbitmq']['vhost'],
      user:  SETTINGS['rabbitmq']['user'],
      pass:  SETTINGS['rabbitmq']['pass']
    )
    conn.start
    conn
  end

  def finalize
    LOGGER.debug 'Finalizing...'
    @rabbitmq.close
    LOGGER.close
  end
end
