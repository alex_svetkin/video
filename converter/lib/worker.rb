require 'streamio-ffmpeg'
require 'pathname'
require 'fileutils'

class Worker
  TIME_LEFT_THRESHOULD = 15 # no time left calculation before that mark, seconds

  attr_reader :workload

  # @param [Message] message
  # @param [Proc] progress_callback
  # @param [Proc] finish_callback
  def initialize(message, progress_callback, finish_callback)
    @workload = message.body.fetch('rendition').fetch('threads', 1).to_i
    @progress_callback = progress_callback
    @thread = create_thread(message, finish_callback)
  end

  # @return [String?]
  def status
    @thread.status
  end

  private

  # @param [Message] message
  # @param [Proc] progress_callback
  # @param [Proc] finish_callback
  def create_thread(message, finish_callback)
    Thread.abort_on_exception = true
    Thread.new do
      movie = get_movie(message.body['source'])
      result = convert_rendition(movie, message.body['rendition'])
      finish_callback.call(message, result)
    end
  end

  # @param [FFMPEG::Movie] movie
  # @param [Hash] rendition
  def convert_rendition(movie, rendition)
    if rendition['type'] == 'video'
      convert_video(movie, rendition)
    elsif rendition['type'] == 'audio'
      convert_audio(movie, rendition)
    else
      raise ArgumentError, "Unknown rendition type: #{rendition['type']}"
    end
  end

  # @param [FFMPEG::Movie] movie
  # @param [Hash] rendition
  def convert_video(movie, rendition)
    if rendition.fetch('two_pass', false)
      LOGGER.debug 'Using two-pass encoding...'
      started_at = Time.now.to_i
      convert_video_subpass(movie, rendition, 1, 2, started_at)
      convert_video_subpass(movie, rendition, 2, 2, started_at)
    else
      LOGGER.debug 'Using single-pass encoding...'
      convert_video_subpass(movie, rendition)
    end
  end

  # @param [FFMPEG::Movie] movie
  # @param [Hash] rendition
  # @param [Integer] pass         Current pass (for multipass encoding)
  # @param [Integer] total_passes Total number of passes
  # @param [Integer] started_at   1-st pass convertation start timestamp (to calculate time left)
  def convert_video_subpass(movie, rendition, pass = 1, total_passes = 1, started_at = nil)
    dest_path = rendition_path(rendition)
    opts = [
      '-an', # no audio
      '-vb', rendition['vb'],
      '-vcodec', 'libx264',
      '-preset:v', rendition['preset'],
      '-vf', format('scale=-2:%d', rendition['height']), # NOTE: -2
      '-force_key_frames', format('expr:gte(t,n_forced*%d)', rendition['gop_size']),
      '-threads', rendition.fetch('threads', 1).to_s
    ]
    passlogfile_prefix = dest_path + '-pass'
    if total_passes > 1
      opts += ['-pass', pass.to_s]
      opts += ['-passlogfile', passlogfile_prefix]
    end

    result = false
    begin
      started_at ||= Time.now.to_i
      movie.transcode(dest_path, opts) do |progress|
        # progress is between 0.0 and 1.0
        actual_progress = (progress + pass - 1) / total_passes * 1.0
        tl = time_left(actual_progress, started_at)
        @progress_callback.call(rendition, actual_progress, tl)
      end
      result = true
    rescue StandardError => e
      LOGGER.error "Convertation failed: #{e}"
    ensure
      FileUtils.rm(Dir.glob(passlogfile_prefix + '*')) if pass == total_passes
    end

    result
  end

  # @param [FFMPEG::Movie] movie
  # @param [Hash] rendition
  def convert_audio(movie, rendition)
    dest_path = rendition_path(rendition)
    opts = [
      '-vn',
      '-ab', rendition['ab'],
      '-ac', '2', # stereo
      '-acodec', 'aac',
      '-strict', '2',
      '-bsf:a', 'aac_adtstoasc',
      '-threads', '1'
    ]
    result = false
    begin
      started_at = Time.now.to_i
      movie.transcode(dest_path, opts) do |progress|
        tl = time_left(progress, started_at)
        @progress_callback.call(rendition, progress, tl)
      end
      result = true
    rescue StandardError => e
      LOGGER.error "Convertation failed: #{e}"
    end

    result
  end

  # calculate time left to convert, based on average transcoding speed
  # @param [Float] progress
  # @param [Integer] started_at
  # @return [Float]
  def time_left(progress, started_at)
    return nil if progress <= 0 || progress > 1
    elapsed = Time.now.to_i - started_at
    return nil if elapsed < TIME_LEFT_THRESHOULD
    (elapsed / progress - elapsed).to_i
  end

  # @param [String] filename
  # @return [FFMPEG::Movie]
  def get_movie(filename)
    path = Pathname.new(SETTINGS['input_path']).join(filename).to_s
    movie = FFMPEG::Movie.new(path)
    LOGGER.debug "Got movie: #{movie.inspect}"
    movie
  end

  # @param [Hash] rendition
  # @return [String]
  def rendition_path(rendition)
    Pathname.new(SETTINGS['output_path']).join(rendition['filename']).to_s
  end
end
