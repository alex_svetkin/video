require './lib/cluster'

class Cluster
  SETTINGS = { 'cpus' => 1 }.freeze
end

RSpec.describe Cluster do
  let(:cluster) { Cluster.new }
  describe '#workload' do
    it 'returns sum of all workers worload' do
      worker = double('worker', workload: 1)
      2.times { cluster.send(:_add_worker, worker) }
      expect(cluster.workload).to eq 2
    end
  end

  describe '#refresh' do
    it 'removes dead workers' do
      worker_ok   = double('worker', workload: 1, status: true)
      worker_dead = double('worker', workload: 1, status: false)
      cluster.send(:_add_worker, worker_ok)
      cluster.send(:_add_worker, worker_dead)
      expect { cluster.refresh! }.to change { cluster.workload }.by(-1)
    end
  end

  describe '#full?' do
    it 'returns true when overload' do
      worker = double('worker', workload: 1)
      2.times { cluster.send(:_add_worker, worker) }
      expect(cluster.full?).to be true
    end
  end
end
