require 'json'
require './lib/message'

RSpec.describe Message do
  describe '.new' do
    it 'parses json payload' do
      message = Message.new({}, {}, '{"a":[1,2,3],"t":"123"}')
      expect(message.body).to eq('a' => [1, 2, 3], 't' => '123')
    end

    it 'raises on invalid payload' do
      expect { Message.new({}, {}, '{"a":[1,2,--invalid') }
        .to raise_error(JSON::ParserError)
    end
  end

  describe '#empty' do
    it 'is empty' do
      expect(Message.new({}, {}, '{}').empty?).to be true
    end
  end

  describe '#delivery_tag' do
    it 'returns delivery tag' do
      message = Message.new({some: 'stuff'}, {}, '{}')
      expect(message.delivery_info).to eq(some: 'stuff')
    end
  end
end
