require './lib/worker'

RSpec.describe Worker do
  let(:message) { Message.new({}, {}, '{"rendition":{"threads":2}}') }
  describe '.new' do
    it 'creates worker' do
      thread = double
      expect_any_instance_of(Worker).to receive(:create_thread).and_return(thread)
      expect(Worker.new(message, -> {}, -> {})).to be_a(Worker)
    end
  end

  describe '#status' do
    it 'returns inner thread status' do
      thread = double(status: '123')
      expect_any_instance_of(Worker).to receive(:create_thread).and_return(thread)
      worker = Worker.new(message, -> {}, -> {})
      expect(worker.status).to eq '123'
    end
  end

  describe '#convert_rendition' do
    let(:worker) do
      thread = double
      expect_any_instance_of(Worker).to receive(:create_thread).and_return(thread)
      Worker.new(message, -> {}, -> {})
    end

    it 'converts video' do
      movie = double
      rendition = { 'type' => 'video' }
      expect(worker).to receive(:convert_video).with(movie, rendition)
      worker.send(:convert_rendition, movie, rendition)
    end

    it 'converts audio' do
      movie = double
      rendition = { 'type' => 'audio' }
      expect(worker).to receive(:convert_audio).with(movie, rendition)
      worker.send(:convert_rendition, movie, rendition)
    end

    it 'raises' do
      movie = double
      rendition = { 'type' => 'unknown' }
      expect { worker.send(:convert_rendition, movie, rendition) }.to raise_error(ArgumentError)
    end
  end
end
