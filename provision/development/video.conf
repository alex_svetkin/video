upstream backend {
  server 127.0.0.1:3000; # puma
}

server {
  listen 4000;
  root /vagrant/backend/public;
  client_max_body_size 500m;
  index index.html;

  proxy_set_header   X-Real-IP  $remote_addr;
  proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header   Host $http_host;
  proxy_redirect     off;
  proxy_read_timeout 30;
  proxy_send_timeout 30s;

  location / {
    try_files $uri @backend;
  }

  location /video/ {
    alias /var/video/;

    # kaltura settings
    vod hls;
    vod_segment_duration 4000;
    vod_align_segments_to_key_frames on;
    vod_hls_force_unmuxed_segments on;

    add_header Access-Control-Allow-Headers '*';
    add_header Access-Control-Expose-Headers 'Server,range,Content-Length,Content-Range';
    add_header Access-Control-Allow-Methods 'GET, HEAD, OPTIONS';
    add_header Access-Control-Allow-Origin '*';
  }

  location @backend {
    proxy_pass http://backend;
  }
}
